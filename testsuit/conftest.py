import pytest


def pytest_addoption(parser):
    parser.addoption("--email", action="store", default="password")
    parser.addoption("--password", action="store", default="password")


@pytest.fixture(scope="session")
def parse_option(pytestconfig):
    email = pytestconfig.getoption("email")
    password = pytestconfig.getoption("password")
    return email, password
