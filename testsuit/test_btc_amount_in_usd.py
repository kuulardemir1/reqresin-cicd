import copy
import json
import smtplib
from datetime import datetime

import requests


def test_btc_amount_in_usd(parse_option):
    email, password = copy.deepcopy(parse_option)
    response = requests.get("https://api.coinbase.com/v2/prices/BTC-USD/spot")
    resp = json.loads(response.text)['data']['amount']
    btc_price_today = f"BTC price {datetime.today()} = {resp}$"
    send_notification([email], btc_price_today, password)


def send_notification(email, txt, password):
    sender = 'BobBobbin2022@yandex.ru'
    sender_password = password
    mail_lib = smtplib.SMTP_SSL('smtp.yandex.ru', 465)
    mail_lib.login(sender, sender_password)
    for to_item in email:
        msg = 'From: %s\r\nTo: %s\r\nContent-Type: text/plain; charset="utf-8"\r\nSubject: %s\r\n\r\n' % (
            sender, to_item, 'check CI/CD working')
        msg += txt
        mail_lib.sendmail(sender, to_item, msg.encode('utf8'))
    mail_lib.quit()


